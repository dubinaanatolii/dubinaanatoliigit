/**
 * Задача 1.
 *
 * Напишите скрипт, который будет убирать все повторяющиеся элементы в массиве, 
 * используя встроенные методы массивов.
 * Новый массив должен содержать все значение без повторений. 
 * 
 * Пример: [1, 2, 3, 3, 4, 2, 1, 5] -> [1, 2, 3, 4, 5]
 *
 * Условия:
 * - Обязательно использовать встроенный метод массива filter;
 *
*/

const arr = [1, 2, 3, 3, 4, 2, 1, 5];
const result = []
// РЕШЕНИЕ


const result1 = arr.filter(function (i) {
    if (!(result.includes(i))) {
      result.push(i)
      console.log('added i' + i)
      return true
    } else { 
      console.log('not added i' + i)
      return false
    }
  });
  console.log(result1);
