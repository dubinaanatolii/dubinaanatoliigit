/*
 * Задача 1.
 *
 * Дописать требуемый функционал что бы код работал правильно.
 * 
 * Необходимо преобразовать первый символ строки в заглавный и вывести эту строку в консоль.
 * Пример 1: привет -> Привет
 * 
 * Пример 2: 222 -> Error.
 *
 * Условия:
 * - Необходимо проверить что введенный текст не число иначе выводить ошибку в консоль.
 */



// РЕШЕНИЕ


const str = prompt('Введите любую строку:'); // ИЗМЕНЯТЬ ЗАПРЕЩЕНО

// type = (typeof(str))
// console.log(type)
// if (type === 'string') {
//     const newstr = str[0].toUpperCase() + str.substring(1);
//     console.log(newstr)
// } else if (type === 'number') {
//     console.log('this is number')}
// else {console.log('error')}

if (Number(str)) {
    console.log('this is number')
}
else {const newstr = str[0].toUpperCase() + str.substring(1);
       console.log(newstr)}